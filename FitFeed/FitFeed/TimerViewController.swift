//
//  TimerViewController.swift
//  FitFeed
//
//  Created by Caleb Barnwell on 12/1/19.
//  Copyright © 2019 FitFeedGroup. All rights reserved.
//

import UIKit

class TimerViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {
    @IBOutlet weak var secPicker: UIPickerView!
    @IBOutlet weak var timeLabel: UILabel!
    
    var timeToWait: Int = -1
    var currentTime: Int = -1
    var paused: Bool = true
    var queue: DispatchQueue = DispatchQueue(label: "timer-queue")
    let numbers = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60]

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        secPicker.delegate = self
        secPicker.dataSource = self

    }
    
    @IBAction func start(_ sender: UIButton) {
        timeToWait = numbers[secPicker.selectedRow(inComponent: 0)]
        currentTime = timeToWait
        paused = false
        self.timeLabel.text = String(self.currentTime)
        
        queue.async{
            while(self.currentTime > 0 && !self.paused){
                sleep(1)
                self.currentTime -= 1
                print(self.currentTime)
                DispatchQueue.main.async{
                    self.timeLabel.text = String(self.currentTime)
                }
            }
            DispatchQueue.main.async{
                self.timeLabel.text = String(self.currentTime)
            }
        }
        
    }
    @IBAction func pause(_ sender: UIButton) {
        paused = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        paused = true
    }
    
    // Used for the picker views
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
        
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return numbers.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return String(numbers[row])
    }

}
