//
//  SecondaryProfileViewController.swift
//  FitFeed
//
//  Created by Laith Alsukhni on 11/6/19.
//  Copyright © 2019 FitFeedGroup. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseFirestore
import FirebaseStorage

class SecondaryProfileViewController: UIViewController {
    
    @IBOutlet weak var titleItem: UINavigationItem!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var bioText: UITextView!
    @IBOutlet weak var followersCount: UILabel!
    @IBOutlet weak var followingCount: UILabel!
    @IBOutlet weak var followButton: UIButton!
    
    var delegate: UIViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let db = Firestore.firestore()
        titleItem.title = secondaryProfileUser
        let temp = db.collection("users").document(secondaryProfileUser)
        temp.getDocument { (snapshot, err) in
            if let data = snapshot?.data() {
                self.titleItem.title=(data["displayName"]! as! String)
            } else {
                print("Couldn't find the document")
            }
        }
        bioText.text = "profile bio"
        followersCount.text = String(-1)
        followingCount.text = String(-1)
        var following_bool = false
        let ref = db.collection("users").document(secondaryProfileUser)
        ref.getDocument { (snapshot, err) in
            if let data = snapshot?.data() {
                self.bioText.text=data["Bio"]! as? String
                
                // loads in followers list if available
                if data["Followers"] != nil {
                    print("followers", (data["Followers"] as! Array<Any>).count)
                    let followers_temp = (data["Followers"] as! Array<Any>) as! [String]
                    if followers_temp.contains((Auth.auth().currentUser?.email)!) {
                        following_bool = true
                    }
                    self.followersCount.text = String((data["Followers"] as! Array<Any>).count)
                }
                else {
                    print("no followers list available")
                }
                // loads in following list if available
                if data["Following"] != nil {
                    print("following", (data["Following"] as! Array<Any>).count)
                    self.followingCount.text = String((data["Following"] as! Array<Any>).count)
                }
                else {
                    print("no following list available")
                }
            } else {
                print("Couldn't find the document")
            }
            if following_bool {
                self.followButton.setTitle("Unfollow", for: .normal)
            }
            else{
                self.followButton.setTitle("Follow", for: .normal)
            }
        }
        
        let storage = Storage.storage()
        let storageRef = storage.reference()
        let userImages = storageRef.child("userImages")
        let imageRef = userImages.child("\(secondaryProfileUser)")
        // Load the image using SDWebImage
        imageRef.getData(maxSize: 1000*1024*1024) { (data, error) in
            if (error != nil) {
                print("FSB")
            } else {
                self.imageView.image = UIImage(data: data!)
            }
        }
    }
    @IBAction func followButton(_ sender: Any) {
        if followButton.title(for: .normal) == "Unfollow" {
            // UNFOLLOW CODE
            var following:[String] = []
            var followers:[String] = []
            let db = Firestore.firestore()
            // update the current user following list
            var ref = db.collection("users").document((Auth.auth().currentUser?.email)!)
            ref.getDocument { (snapshot, err) in
                if let data = snapshot?.data() {
                    // loads in followers list if available
                    following = (data["Following"] as! Array<Any>) as! [String]
                    following = following.filter {$0 != secondaryProfileUser}
                    db.collection("users").document((Auth.auth().currentUser?.email)!).updateData(["Following" : following])
                } else {
                    print("Couldn't find the document")
                }
            }
            // update secondary user followers list
            ref = db.collection("users").document(secondaryProfileUser)
            ref.getDocument { (snapshot, err) in
                if let data = snapshot?.data() {
                    // loads in followers list if available
                    followers = (data["Followers"] as! Array<Any>) as! [String]
                    followers = followers.filter {$0 != (Auth.auth().currentUser?.email)!}
                    db.collection("users").document(secondaryProfileUser).updateData(["Followers" : followers])
                    let new_count = Int(self.followersCount.text!)! - 1
                    self.followersCount.text = String(new_count)
                } else {
                    print("Couldn't find the document")
                }
            }
            self.followButton.setTitle("Follow", for: .normal)
        }
        else {
            // FOLLOW CODE
            var following:[String] = []
            var followers:[String] = []
            let db = Firestore.firestore()
            // update the current user following list
            var ref = db.collection("users").document((Auth.auth().currentUser?.email)!)
            ref.getDocument { (snapshot, err) in
                if let data = snapshot?.data() {
                    // loads in followers list if available
                    following = (data["Following"] as! Array<Any>) as! [String]
                    following.append(secondaryProfileUser)
                    db.collection("users").document((Auth.auth().currentUser?.email)!).updateData(["Following" : following])
                } else {
                    print("Couldn't find the document")
                }
            }
            // update secondary user followers list
            ref = db.collection("users").document(secondaryProfileUser)
            ref.getDocument { (snapshot, err) in
                if let data = snapshot?.data() {
                    // loads in followers list if available
                    followers = (data["Followers"] as! Array<Any>) as! [String]
                    followers.append((Auth.auth().currentUser?.email)!)
                    db.collection("users").document(secondaryProfileUser).updateData(["Followers" : followers])
                    let new_count = Int(self.followersCount.text!)! + 1
                    self.followersCount.text = String(new_count)
                } else {
                    print("Couldn't find the document")
                }
            }
            self.followButton.setTitle("Unfollow", for: .normal)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        secondaryProfileUser = ""
        if mainProfileReloadVar {
            let prevVC = delegate as! updateLists
            prevVC.updateLists()
        }
    }
}
    
