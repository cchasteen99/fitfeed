//
//  editProfileViewController.swift
//  FitFeed
//
//  Created by Christopher Chasteen on 10/22/19.
//  Copyright © 2019 FitFeedGroup. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import FirebaseFirestore
import FirebaseStorage


class editProfileViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    @IBOutlet weak var usernameField: UITextField!
    @IBOutlet weak var bioField: UITextView!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var backPressed: UIBarButtonItem!
    var imagePicker = UIImagePickerController()

    @IBAction func backPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        let db = Firestore.firestore()
        let user = Auth.auth().currentUser?.email!
        let ref = db.collection("users").document(user!)
        ref.getDocument { (snapshot, err) in
            if let data = snapshot?.data() {
                self.bioField.text=(data["Bio"]! as! String)
                self.usernameField.text=(data["displayName"]! as! String)
            } else {
                print("Couldn't find the document")
            }
        }
        
        let storage = Storage.storage()
        let storageRef = storage.reference()
        let userImages = storageRef.child("userImages")
        let imageRef = userImages.child("\(user!)")
        // Load the image using SDWebImage
        imageRef.getData(maxSize: 1000*1024*1024) { (data, error) in
            if (error != nil) {
                print("FSB")
            } else {
                self.profileImageView.image = UIImage(data: data!)
            }
        }
        
    }
    
    @IBAction func useCameraButton(_ sender: Any) {
        if UIImagePickerController.isSourceTypeAvailable(.camera){
           // print("Change Photo")

            imagePicker.delegate = self
            imagePicker.sourceType = .camera
            imagePicker.allowsEditing = true

            present(imagePicker, animated: true, completion: nil)
        }
    }
    @IBAction func changePhotoButton(_ sender: Any) {
            if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum){
               // print("Change Photo")

                imagePicker.delegate = self
                imagePicker.sourceType = .savedPhotosAlbum
                imagePicker.allowsEditing = true

                present(imagePicker, animated: true, completion: nil)
            }
        }
    
    

    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
         let image = info[.editedImage] as? UIImage
         profileImageView.image = image
        self.dismiss(animated: true, completion: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        let db = Firestore.firestore()
        let user = Auth.auth().currentUser?.email!
        //print("USER IS "  , user!)
        db.collection("users").document(user!).updateData([
            "Bio": bioField.text!
        ])
        //Store the image and add a reference to the user account
        let storage = Storage.storage()
        let storageRef = storage.reference()
        let userImages = storageRef.child("userImages")
        let imageRef = userImages.child("\(user!)")
        let Data = (profileImageView.image?.pngData())!
        db.collection("users").document(user!).setData([
            "displayName": usernameField.text!
        ], merge: true)
        //Create Upload Task
        imageRef.putData(Data, metadata: nil)

    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
