//
//  Subworkout.swift
//  FitFeed
//  EID: cmb5827
//  Course: CS371L
//
//  Created by Caleb Barnwell on 10/20/19.
//  Copyright © 2019 FitFeedGroup. All rights reserved.
//

import Foundation

// Subworkout Class for database
public class Subworkout: NSObject, NSCoding{
    public var title: String = ""
    public var sets: String = "0"
    public var reps: String = "0"
    
    enum Key:String{
        case title = "title"
        case sets = "sets"
        case reps = "reps"
    }
    
    init(title: String, sets: String, reps: String){
        self.title = title
        self.sets = sets
        self.reps = reps
    }
    
    public override init(){
        super.init()
    }
    
    public func encode(with aCoder: NSCoder){
        aCoder.encode(title, forKey: Key.title.rawValue)
        aCoder.encode(sets, forKey: Key.sets.rawValue)
        aCoder.encode(reps, forKey: Key.reps.rawValue)
    }
    
    public required convenience init?(coder aDecoder: NSCoder){
        let mTitle = aDecoder.decodeObject(forKey: Key.title.rawValue) as! String
        let mSets = aDecoder.decodeObject(forKey: Key.sets.rawValue) as! String
        let mReps = aDecoder.decodeObject(forKey: Key.reps.rawValue) as! String
        
        self.init(title: String(mTitle), sets: mSets, reps: mReps)
    }
}

// Subworkouts Class for database, to hold array of it
public class Subworkouts: NSObject, NSCoding{
    public var subworkouts: [Subworkout] = []
    
    enum Key:String{
        case subworkouts = "subworkouts"
    }
    
    init(subworkouts: [Subworkout]){
        self.subworkouts = subworkouts
    }
    
    public override init(){
        super.init()
    }
    
    public func encode(with aCoder: NSCoder){
        aCoder.encode(subworkouts, forKey: Key.subworkouts.rawValue)
    }
    
    public required convenience init?(coder aDecoder: NSCoder){
        let mSubworkouts = aDecoder.decodeObject(forKey: Key.subworkouts.rawValue) as! [Subworkout]
        
        self.init(subworkouts: mSubworkouts)
    }
}

// Used for displaying
class WorkoutDisplay{
    var title: String
    var description: String
    var workoutList: [Subworkout] = []

    init(title: String, description: String){
        self.title = title
        self.description = description
    }
    
    func setList(sub: [Subworkout]){
        workoutList = sub
    }
}
