//
//  settingsViewController.swift
//  FitFeed
//
//  Created by Christopher Chasteen on 10/24/19.
//  Copyright © 2019 FitFeedGroup. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseFirestore
class settingsViewController: UIViewController {
    
    @IBAction func backPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
      
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "signOutSegue" {
            //print("signing Out")
                   do {
                       try Auth.auth().signOut()
                   } catch let signOutError as NSError {
                       print ("Error signing out: \(signOutError)")
                   } catch {
                       print("Unknown error.")
                   }
        }
        if segue.identifier == "deleteAccountSegue" {
            let user = Auth.auth().currentUser
            let db = Firestore.firestore()
            db.collection("users").document((user?.email!)!).delete() { err in
                    if let err = err {
                        print("Error removing document: \(err)")
                    }
            }
            db.collection("posts").getDocuments{ (snapshot, err) in
            if let err = err {
               print("Error getting documents: \(err)")
               } else {
               for document in snapshot!.documents {
                  let author = document.get("author") as! String
                if author == user?.email! {
                    db.collection("posts").document(document.documentID).delete() { err in
                            if let err = err {
                                print("Error removing document: \(err)")
                            }
                    }
                }
                }}}
            
            user?.delete { error in
             if let error = error {
                print(error.localizedDescription)
             } else {
               print("Unknown error")
             }
            }
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
