//
//  EditWorkoutViewController.swift
//  FitFeed
//
//  Created by Caleb Barnwell on 11/27/19.
//  Copyright © 2019 FitFeedGroup. All rights reserved.
//

import UIKit

import UIKit
import CoreData
import Firebase
import FirebaseFirestore

protocol EditWorkoutDelegate {
    var editWorkoutTitle: String {get set}
    var editWorkoutDescription: String {get set}
    
    var subworkoutList: [Subworkout] {get set}
}

class EditWorkoutViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
//    @IBOutlet weak var workoutTitle: UITextField!
//    @IBOutlet weak var descriptionField: UITextView!
//    @IBOutlet weak var subTable: UITableView!
//    @IBOutlet weak var addImageButton: UIButton!
    @IBOutlet weak var workoutTitle: UITextField!
    @IBOutlet weak var descriptionField: UITextView!
    @IBOutlet weak var addImageButton: UIButton!
    @IBOutlet weak var subTable: UITableView!
    
    var subworkoutList: [Subworkout] = []
    var indexList:[Int] = []
    var cellCount: Int = 0
    var fetched: Bool = false
    let imagePicker = UIImagePickerController()
    var image: UIImage = UIImage()
    var imageAdded: Bool = false
    var dataReady: Bool = false
    var newSubworkoutList: [Subworkout] = []
    
    var delegate:EditWorkoutDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        subTable.rowHeight = 90
        subTable.delegate = self
        subTable.dataSource = self
        imagePicker.delegate = self
        if(delegate != nil){
            subworkoutList = delegate!.subworkoutList
        }
        populateTable()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if(delegate != nil){
            workoutTitle.text = delegate!.editWorkoutTitle
            descriptionField.text = delegate!.editWorkoutDescription
        }
    }
    @IBAction func addSubworkout(_ sender: Any) {
//        addAllCells()
        cellCount += 1
        subTable.reloadData()
    }
    @IBAction func addImage(_ sender: Any) {
        imagePicker.allowsEditing = false
        imagePicker.sourceType = .photoLibrary
            
        present(imagePicker, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            image = pickedImage
        }
     
        dismiss(animated: true, completion: nil)
        imageAdded = true
        addImageButton.setTitle("Change Image", for: .normal)
    }
    
    override func viewDidAppear(_ animated: Bool) {
//        cellCount = 0
//        subworkoutList = []
        indexList = []
        repsArr = []
        setsArr = []
//        descriptionField.text = ""
//        workoutTitle.text = ""
        
//        for i in 0 ... subworkoutList.count - 1{
//            setsArr.insert(subworkoutList[i].sets, at: i)
//            repsArr.insert(subworkoutList[i].reps, at: i)
////            indexList.insert(i, at: i)
//        }

        subTable.reloadData()

    }
    
    @IBAction func editSave(_ sender: UIButton) {
        fetched = false
        //subTable.reloadData()

        if workoutTitle.text != ""{
            print("workout title not nil")
                let db = Firestore.firestore()
                    let user = Auth.auth().currentUser?.email!
                    let docTitle = "\(workoutTitle.text!)-\(user!)"
                db.collection("users/\(user!)/workouts").document(docTitle).delete() { err in
                        if let err = err {
                            print("Error removing document: \(err)")
                        }
                    }
            print("deleted original workout")
            
                    let cells = self.subTable.visibleCells as! Array<CreateWorkoutTableViewCell>
                    for cell in cells {
                        newSubworkoutList = []
                        newSubworkoutList.append(Subworkout(title: cell.titleField.text!, sets: cell.setField.text!, reps: cell.repField.text!))
            //            newSubworkoutList[i].title = cell.titleField.text!
            //            newSubworkoutList[i].sets = Int(cell.setField.text!)!
            //            newSubworkoutList[i].reps = Int(cell.repField.text!)!
                    }
                

                db.collection("users/\(user!)/workouts").document(docTitle).setData([
                    "title": workoutTitle.text!, "user": user!, "description": descriptionField.text!, "image_url": "www.fakeimageurl.zzz"
                ])
                for i in 0 ..< newSubworkoutList.count{
                    if(newSubworkoutList[i].title != ""){
                    db.collection("users/\(user!)/workouts/\(docTitle)/subworkouts").document("\(i)").setData(["title": newSubworkoutList[i].title, "sets": newSubworkoutList[i].sets, "reps": newSubworkoutList[i].reps])
                    }
                }
                if(imageAdded){
                    print("image is new")
                    let storage = Storage.storage()
                    let storageRef = storage.reference()
                    let workoutImages = storageRef.child("workoutImages")
                    let userImageFolder = workoutImages.child("\(user!)")
                    let imageRef = userImageFolder.child("\(workoutTitle.text!)")
                    let Data = (image.pngData())!
                    imageRef.putData(Data, metadata: nil)
                }
                else{
                    print("image is the same")
                    // CONVERT TO IMAGE WITH NEW WORKOUT NAME
                }
            print("Added new workout")
            self.dismiss(animated: true, completion: nil)
        }
        else {
            let alert = UIAlertController(title: "Error", message: "Text Fields Cannot be blank", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
            self.present(alert, animated: true)
        }
    }
    
    @IBAction func clearPress(_ sender: Any) {
        viewDidAppear(false)
    }
//    func addAllCells() {
//        let cells = self.subTable.visibleCells as! Array<CreateWorkoutTableViewCell>
//
//        for cell in cells {
//            let index = subTable.indexPath(for: cell)?.row
//            if(delegate!.subworkoutList.count > 0){
//            for i in 0 ... delegate!.subworkoutList.count - 1 {
//                if indexList[i] == index! {
//                delegate!.subworkoutList.remove(at: i)
//                    indexList.remove(at: i)
//                    break
//                }
//            }
//            }
//            if(index! <= indexList.count){
//            let setText = cell.setField.text!
//            let sets = Int(setText)
//
//            let repText = cell.repField.text!
//            let reps = Int(repText)
//
//            indexList.insert(index!, at: index!)
//                delegate!.subworkoutList.insert(Subworkout(title: cell.titleField.text!, sets: sets!, reps: reps!), at: 0)
//                repsArr.insert(reps!, at: 0)
//                setsArr.insert(sets!, at: 0)
//            }}
//    }
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
//                addAllCells()
    }
    
    func populateTable(){
        let db = Firestore.firestore()
        let user = Auth.auth().currentUser?.email!
        print("Searching for: \(delegate!.editWorkoutTitle)-\(user!)")
        let subworkouts =
            db.collection("users/\(user!)/workouts/\(delegate!.editWorkoutTitle)-\(user!)/subworkouts")
        subworkouts.getDocuments() { (querySnapshot, err) in
                    if let err = err {
                        print("Error getting documents: \(err)")
                    } else {
                        for subDocument in querySnapshot!.documents {
                            let subToAdd = Subworkout(title: subDocument.data()["title"] as! String, sets: subDocument.data()["sets"] as! String, reps: subDocument.data()["reps"] as! String)
                            print("Sub to add: \(subToAdd)")
                            self.newSubworkoutList.append(subToAdd)
                        }
                        self.cellCount = self.newSubworkoutList.count
                        print("Subworkout count \(self.newSubworkoutList.count)")
                        self.subTable.reloadData()
                    }
                }
            print("NewSubList: \(newSubworkoutList)")
            self.dataReady = true
            
    }
}



extension EditWorkoutViewController: UITableViewDataSource, UITableViewDelegate{
    
    // Return the number of rows for the table.
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cellCount
    }
    
    // Provide a cell object for each row.
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // Fetch cell
        if(indexPath.row == 0){
            fetched = false
        }
        let cell = subTable.dequeueReusableCell(withIdentifier: "CreateWorkoutTableViewCell") as! CreateWorkoutTableViewCell
        
        
       // print("Cell Sets Reps \(cell.setPicker.selectedRow(inComponent: 0)) \(cell.setPicker.selectedRow(inComponent: 0))")
        //print(indexList.count)
        
        print("setting")
        cell.titleField.text = newSubworkoutList[indexPath.row].title
        cell.setField.text = String(newSubworkoutList[indexPath.row].sets)
        cell.repField.text = String(newSubworkoutList[indexPath.row].reps)
        return cell
    }
    
    // Cell has been selected
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
}
