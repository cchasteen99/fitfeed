//
//  FollowListViewController.swift
//  FitFeed
//
//  Created by Laith Alsukhni on 11/20/19.
//  Copyright © 2019 FitFeedGroup. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseFirestore
import FirebaseStorage

var followListUser = ""
var followListState = ""
var followersList:[String] = []
var followingList:[String] = []

protocol updateLists {
    func updateLists()
}

class FollowListViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, updateLists {
    
    @IBOutlet weak var titleItem: UINavigationItem!
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    @IBOutlet weak var tableView: UITableView!
    
    override func viewWillAppear(_ animated: Bool) {

        if followListState == "followers" {
            segmentedControl.selectedSegmentIndex = 0
        }
        
        if followListState == "following" {
            segmentedControl.selectedSegmentIndex = 1
        }
        
        setupLists()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        let Q = DispatchQueue(label: "Q-name", qos: .utility)
        Q.async {
            sleep(1)
            DispatchQueue.main.async {
                self.setup()
           }
        }
    }
    
    func setup () {
        titleItem.title = followListUser
        tableView.reloadData()
    }
    
    func setupLists () {
        followersList = []
        followingList = []
        
        let db = Firestore.firestore()
        let ref = db.collection("users").document((Auth.auth().currentUser?.email!)!)
        
        ref.getDocument { (snapshot, err) in
            if let data = snapshot?.data() {
                // loads in followers list if available
                if data["Followers"] != nil {
                    print("followers", (data["Followers"] as! Array<Any>).count)
                    
                    let followers_temp = (data["Followers"] as! Array<Any>) as! [String]
                    print(followers_temp)
                    followersList = (data["Followers"] as! Array<Any>) as! [String]
                    print(followersList)
                }
                else {
                    print("no followers list available")
                }
                // loads in following list if available
                if data["Following"] != nil {
                    print("following", (data["Following"] as! Array<Any>).count)
                    
                    let followers_temp = (data["Following"] as! Array<Any>) as! [String]
                    print(followers_temp)
                    followingList = (data["Following"] as! Array<Any>) as! [String]
                }
                else {
                    print("no following list available")
                }
            }
        }
    }
    
    func updateLists() {
        let Q = DispatchQueue(label: "Q-name", qos: .utility)
        Q.async {
            DispatchQueue.main.async {
                self.setupLists()
            }
            sleep(1)
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // when clicking a cell, it will go to the profile and pass along the user
        tableView.deselectRow(at: indexPath, animated: true)

        switch followListState
        {
        case "followers":
            secondaryProfileUser = followersList[indexPath.row]
        case "following":
            secondaryProfileUser = followingList[indexPath.row]
        default:
            break
        }
        
        performSegue(withIdentifier: "secondProfileSeg", sender: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "secondProfileSeg",
            let nextVC = segue.destination as? SecondaryProfileViewController {
                nextVC.delegate = self
            }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch followListState
        {
        case "followers":
            return followersList.count
        case "following":
            return followingList.count
        default:
            break
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let db = Firestore.firestore()
        let cell = tableView.dequeueReusableCell(withIdentifier: "followCell", for: indexPath) as! FollowListTableViewCell
        let row = indexPath.row
        var username = ""
        
        cell.profileImage.image = UIImage(named: "blank-profile-picture.png")

        switch followListState
        {
        case "followers":
            username = followersList[row]
        case "following":
            username = followingList[row]
        default:
            break
        }
                
        let ref = db.collection("users").document(username)
                           ref.getDocument { (snapshot, err) in
                               if let data = snapshot?.data() {
                                cell.username.text = (data["displayName"]! as! String)
                               }
                           }
        
        let storage = Storage.storage()
        let storageRef = storage.reference()
        let userImages = storageRef.child("userImages")
        let imageRef = userImages.child("\(username)")
        // Load the image using SDWebImage
        imageRef.getData(maxSize: 1000*1024*1024) { (data, error) in
            if (error != nil) {
                print("No profile pic associated with user \(username)")
                cell.profileImage.image = UIImage(named: "blank-profile-picture.png")
            } else {
                cell.profileImage.image = UIImage(data: data!)
            }
        }
        return cell
    }
    
    @IBAction func segControlChange(_ sender: Any) {
        switch segmentedControl.selectedSegmentIndex
        {
        case 0:
            followListState = "followers"
        case 1:
            followListState = "following"
        default:
            break
        }
        tableView.reloadData()
    }
    
    @IBAction func backButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        followListState = ""
        mainProfileReloadVar = false
    }
}
