//
//  SearchViewController.swift
//  FitFeed
//
//  Created by Laith Alsukhni on 11/4/19.
//  Copyright © 2019 FitFeedGroup. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseFirestore
import FirebaseStorage

var searchList:[String] = []
var searchListFiltered:[String] = []
var searching = false
var secondaryProfileUser = ""

class SearchViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {
    @IBOutlet weak var searchTableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    override func viewWillAppear(_ animated: Bool) {
        let db = Firestore.firestore()
        let users = db.collection("users")
        users.getDocuments() { (querySnapshot, err) in
            if let err = err {
                print("Error getting documents: \(err)")
            }
            else {
                searchList = []
                for document in querySnapshot!.documents {
                    if(Auth.auth().currentUser!.email! != document.documentID){
                        searchList.append("\(document.documentID)")
                    }
                }
                self.searchTableView.reloadData()
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        searchTableView.delegate = self
        searchTableView.dataSource = self
        searchBar.delegate = self
        searchTableView.reloadData()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        searching = true
        searchListFiltered = searchList.filter({$0.prefix(searchText.count).lowercased() == searchText.lowercased()})
        searchTableView.reloadData()
    }
    
    // SearchBar functions
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.view.endEditing(true)
        searching = false
        searchListFiltered = []
        searchBar.text = ""
        searchTableView.reloadData()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.view.endEditing(true)
        searchTableView.reloadData()
    }
    
    // TableView functions
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // when clicking a cell, it will go to the profile and pass along the user
        searchTableView.deselectRow(at: indexPath, animated: true)
        if searching {
            secondaryProfileUser = searchListFiltered[indexPath.row]
        }
        else {
            secondaryProfileUser = searchList[indexPath.row]
        }
        performSegue(withIdentifier: "otherProfileSeg", sender: nil)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if searching {
            return searchListFiltered.count
        }
        else {
            return searchList.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let db = Firestore.firestore()
        let cell = tableView.dequeueReusableCell(withIdentifier: "searchCell", for: indexPath) as! SearchTableViewCell
        let row = indexPath.row
        var username = ""
        if searching {
            username = searchListFiltered[row]
        }
        else {
            username = searchList[row]
        }
        let ref = db.collection("users").document(username)
                           ref.getDocument { (snapshot, err) in
                               if let data = snapshot?.data() {
                                cell.userName.text = (data["displayName"]! as! String)
                               } else {
                                 
                               }
                           }
        
        let storage = Storage.storage()
        let storageRef = storage.reference()
        let userImages = storageRef.child("userImages")
        let imageRef = userImages.child("\(username)")
        // Load the image using SDWebImage
        imageRef.getData(maxSize: 1000*1024*1024) { (data, error) in
            if (error != nil) {
//                print("No profile pic associated with user \(username)")
                cell.profilePic.image = UIImage(named: "blank-profile-picture.png")
            } else {
                cell.profilePic.image = UIImage(data: data!)
            }
        }
        return cell
    }
}
