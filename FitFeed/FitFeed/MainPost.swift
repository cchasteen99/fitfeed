//
//  MainPost.swift
//  FitFeed
//
//  Created by Ryan Gutierrez on 11/1/19.
//  Copyright © 2019 FitFeedGroup. All rights reserved.
//

import Foundation
import UIKit

public class MainPost{
    var author:String
    var displayName:String
    var workout:String
    var descript:String
    var subworkouts:[String]
    var reps:[String]
    var sets:[String]
    var timeStamp:Date
    
    init(author: String,displayName: String, workout: String, descript:String, subworkouts:[String], reps: [String], sets: [String],
         timeStamp: Date){
        self.author = author
        self.workout = workout
        self.descript = descript
        self.displayName = displayName
        self.subworkouts = subworkouts
        self.reps = reps
        self.sets = sets
        self.timeStamp = timeStamp
        
    }
    
    
    
    
}
