//
//  SplashViewController.swift
//  FitFeed
//
//  Created by Ryan Gutierrez on 10/23/19.
//  Copyright © 2019 FitFeedGroup. All rights reserved.
//

import UIKit

class SplashViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        DispatchQueue.main.asyncAfter(deadline:.now() + 3.0, execute: {
           self.performSegue(withIdentifier:"splashSegue",sender: self)
        })
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
