//
//  WorkoutListingViewController.swift
//  FitFeed
//
//  Created by Caleb Barnwell on 10/20/19.
//  Copyright © 2019 FitFeedGroup. All rights reserved.
//

import UIKit
import CoreData
import Firebase
import FirebaseFirestore
var workoutListing: [WorkoutDisplay] = []

class WorkoutListingViewController: UIViewController, WorkoutViewerDelegate {
    var workoutViewerTitle: String  = ""
    var workoutViewerSubTitle: String = ""
    var workoutViewerSets: String = ""
    var workoutViewerReps: String = ""
    var workoutViewerDescription: String = ""
    var subworkoutList: [Subworkout] = []
    var dataReady: Bool = false
    
    @IBOutlet weak var workoutTitle: UILabel!
    @IBOutlet weak var workoutListTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        workoutListTableView.delegate = self
        workoutListTableView.dataSource = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        workoutListing = []
        subworkoutList = []
        self.populateTable()
    }
    
    override func viewDidAppear(_ animated: Bool) {
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        dataReady = false
    }
    
    // Populate table from database
    func populateTable(){
        let db = Firestore.firestore()
        let user = Auth.auth().currentUser?.email!
        
        let workouts = db.collection("users/\(user!)/workouts")
        workouts.getDocuments() { (querySnapshot, err) in
        if let err = err {
            print("Error getting documents: \(err)")
        } else {
            for document in querySnapshot!.documents {
                workoutListing.insert(WorkoutDisplay(title: document.data()["title"] as! String, description: document.data()["description"] as! String), at: 0)
                let subworkouts = db.collection("users/\(user!)/workouts/\(document.data()["title"] as! String)-\(user!)/subworkouts")
                subworkouts.getDocuments() { (subQuerySnapshot, err) in
                    if let err = err {
                        print("Error getting documents: \(err)")
                    } else {
                        for subDocument in subQuerySnapshot!.documents {
                            let subToAdd = Subworkout(title: subDocument.data()["title"] as! String, sets: subDocument.data()["sets"] as! String, reps: subDocument.data()["reps"] as! String)
                        }
                    }
                }
            }
            self.dataReady = true
            self.workoutListTableView.reloadData()
        }
        }
    }
    
    // Prepare to display
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "workoutViewSegue" {
            let vc : WorkoutViewerViewController = segue.destination as! WorkoutViewerViewController
            vc.delegate = self
        }
    }
}

extension WorkoutListingViewController: UITableViewDataSource, UITableViewDelegate{
    
    // Return the number of rows for the table.
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return workoutListing.count
    }
    
    // Provide a cell object for each row.
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = workoutListTableView.dequeueReusableCell(withIdentifier: "WorkoutListingTableViewCell") as! WorkoutListingTableViewCell
        
        let workout = workoutListing[indexPath.row]

        cell.title.text = workout.title
        return cell
    }
    
    // Cell has been selected
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if workoutListing.count > 0{
        workoutViewerTitle = workoutListing[indexPath.row].title
        workoutViewerDescription = workoutListing[indexPath.row].description
            
    
        
        //Add the workouts needed only for the selected workout @chris
        
        let db = Firestore.firestore()
        let user = Auth.auth().currentUser?.email!



        let subworkouts = db.collection("users/\(user!)/workouts/\(workoutListing[indexPath.row].title)-\(user!)/subworkouts")
        subworkouts.getDocuments() { (subQuerySnapshot, err) in
                            if let err = err {
                                print("Error getting documents: \(err)")
                            } else {
                                for subDocument in subQuerySnapshot!.documents {
                                    //print("\(subDocument.documentID) => \(subDocument.data())")
                                    let subToAdd = Subworkout(title: subDocument.data()["title"] as! String, sets: subDocument.data()["sets"] as! String, reps: subDocument.data()["reps"] as! String)
                                    //print("sub to add: \(subToAdd.title)")
                                    self.subworkoutList.append(subToAdd)
                                }
            }}
        self.dataReady = true
        self.workoutListTableView.reloadData()
        //print("LIST IS")
        //print(subworkoutList)
        performSegue(withIdentifier: "workoutViewSegue", sender: workoutListTableView)
        
        }
        
    }
}
