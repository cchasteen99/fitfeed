//
//  CreateWorkoutViewController.swift
//  FitFeed
//  EID: cmb5827
//  Course: CS371L
//
//  Created by Caleb Barnwell on 10/20/19.
//  Copyright © 2019 FitFeedGroup. All rights reserved.
//

import UIKit
import CoreData
import Firebase
import FirebaseFirestore

class CreateWorkoutViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    @IBOutlet weak var workoutTitle: UITextField!
    @IBOutlet weak var descriptionField: UITextView!
    @IBOutlet weak var subTable: UITableView!
    @IBOutlet weak var addImageButton: UIButton!
    @IBOutlet weak var imageField: UIImageView!
    
    var subworkoutList: [Subworkout] = []
    var indexList:[Int] = []
    var cellCount: Int = 0
    var fetched: Bool = false
    let imagePicker = UIImagePickerController()
    var image: UIImage = UIImage()
    var imageAdded: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        subTable.rowHeight = 90
        subTable.delegate = self
        subTable.dataSource = self
        imagePicker.delegate = self
    }
    @IBAction func addSubworkout(_ sender: Any) {
        addAllCells()
        cellCount += 1
        subTable.reloadData()
    }
    @IBAction func addImage(_ sender: Any) {
        imagePicker.allowsEditing = false
        imagePicker.sourceType = .photoLibrary
            
        present(imagePicker, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            image = pickedImage
            imageField.image = pickedImage
        }
     
        dismiss(animated: true, completion: nil)
        imageAdded = true
        addImageButton.setTitle("Change Image", for: .normal)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        cellCount = 0
        subworkoutList = []
        indexList = []
        repsArr = []
        setsArr = []
        descriptionField.text = ""
        workoutTitle.text = ""
        
        imageAdded = false
        addImageButton.setTitle("Add Image", for: .normal)
        imageField.image = UIImage(named: "workoutImage.jpg")
        subTable.reloadData()

    }
    
    // When save is clicked, save to database
    @IBAction func save(_ sender: Any) {
        print("Saving")
        addAllCells()
        fetched = false
        subTable.reloadData()

        if workoutTitle.text != ""{
            if imageAdded == true{
                let db = Firestore.firestore()
                let user = Auth.auth().currentUser?.email!
                let docTitle = "\(workoutTitle.text!)-\(user!)"
                

                db.collection("users/\(user!)/workouts").document(docTitle).setData([
                    "title": workoutTitle.text!, "user": user!, "description": descriptionField.text!, "image_url": "www.fakeimageurl.zzz"
                ])
                for i in 0 ..< subworkoutList.count{
                    if(subworkoutList[i].title != ""){
                    db.collection("users/\(user!)/workouts/\(docTitle)/subworkouts").document("\(i)").setData(["title": subworkoutList[i].title, "sets": subworkoutList[i].sets, "reps": subworkoutList[i].reps])
                    }
                }
                let storage = Storage.storage()
                let storageRef = storage.reference()
                let workoutImages = storageRef.child("workoutImages")
                let userImageFolder = workoutImages.child("\(user!)")
                let imageRef = userImageFolder.child("\(workoutTitle.text!)")
                let Data = (image.pngData())!
                    imageRef.putData(Data, metadata: nil)
            }else{
            
                let db = Firestore.firestore()
                let user = Auth.auth().currentUser?.email!
                let docTitle = "\(workoutTitle.text!)-\(user!)"
                

                db.collection("users/\(user!)/workouts").document(docTitle).setData([
                    "title": workoutTitle.text!, "user": user!, "description": descriptionField.text!, "image_url": "www.fakeimageurl.zzz"
                ])
                for i in 0 ..< subworkoutList.count{
                    if(subworkoutList[i].title != ""){
                    db.collection("users/\(user!)/workouts/\(docTitle)/subworkouts").document("\(i)").setData(["title": subworkoutList[i].title, "sets": subworkoutList[i].sets, "reps": subworkoutList[i].reps])
                    }
                }
                let storage = Storage.storage()
                let storageRef = storage.reference()
                let workoutImages = storageRef.child("workoutImages")
                let userImageFolder = workoutImages.child("\(user!)")
                let imageRef = userImageFolder.child("\(workoutTitle.text!)")
                let Data = (imageField.image!.pngData())!
                    imageRef.putData(Data, metadata: nil)
            }
        }
        else {
            let alert = UIAlertController(title: "Error", message: "Text Fields Cannot be blank", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
            self.present(alert, animated: true)
        }
        
    }
    
    @IBAction func clearPress(_ sender: Any) {
        viewDidAppear(false)
        
    }
    func addAllCells() {
        let cells = self.subTable.visibleCells as! Array<CreateWorkoutTableViewCell>

        for cell in cells {
            let index = subTable.indexPath(for: cell)?.row
            if(subworkoutList.count > 0){
            for i in 0 ... subworkoutList.count - 1 {
                if indexList[i] == index! {
                subworkoutList.remove(at: i)
                    indexList.remove(at: i)
                    break
                }
            }
            }
            if(index! <= indexList.count){
            let setText = cell.setField.text!
            let sets = setText
            
            let repText = cell.repField.text!
            let reps = repText
                
            indexList.insert(index!, at: index!)
                subworkoutList.insert(Subworkout(title: cell.titleField.text!, sets: sets, reps: reps), at: 0)
                repsArr.insert(reps, at: 0)
                setsArr.insert(sets, at: 0)
            }}
    }
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
                addAllCells()
    }

    
    // When post is clicked
    @IBAction func post(_ sender: Any) {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let date = Date()
        let fd = formatter.string(from: date)
        save(self)
        fetched = false
                subTable.reloadData()

                if workoutTitle.text != "" {
                    let db = Firestore.firestore()
                    let user = Auth.auth().currentUser?.email!
                    let docTitle = "\(workoutTitle.text!)-\(user!)"
                    
                    db.collection("posts").document(docTitle).setData([
                        "title": workoutTitle.text!, "author": user!,
                        "description": descriptionField.text!, "image_url": "www.fakeimageurl.zzz",
                        "timestamp": fd
                    ])
                    for i in 0 ..< subworkoutList.count{
                        db.collection("posts/\(docTitle)/subworkouts").document("\(subworkoutList[i].title)-\(i)").setData(["title": subworkoutList[i].title, "sets": subworkoutList[i].sets, "reps": subworkoutList[i].reps])
                    }
                }
                else {
                    let alert = UIAlertController(title: "Error", message: "Text Fields Cannot be blank", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
                    self.present(alert, animated: true)
                }
    }
}



extension CreateWorkoutViewController: UITableViewDataSource, UITableViewDelegate{
    
    // Return the number of rows for the table.
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cellCount
    }
    
    // Provide a cell object for each row.
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // Fetch cell
        if(indexPath.row == 0){
            fetched = false
        }
        let cell = subTable.dequeueReusableCell(withIdentifier: "CreateWorkoutTableViewCell") as! CreateWorkoutTableViewCell
        
        
        if(indexList.count > 0){
            if(indexList.contains(indexPath.row)){
                for i in 0 ... indexList.count - 1 {
                    if indexList[i] == indexPath.row {
                        cell.titleField.text = subworkoutList[i].title
                        cell.setField.text = String(setsArr[i])
                        cell.repField.text = String(repsArr[i])
                    }
                }
            }
            else {
                cell.titleField.text = ""
                cell.setField.text = ""
                cell.repField.text = ""
            }
            
            }
        else{
            cell.titleField.text = ""
            cell.setField.text = ""
            cell.repField.text = ""
        }
        return cell
    }
    
    // Cell has been selected
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
}
