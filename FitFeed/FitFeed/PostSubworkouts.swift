//
//  PostSubworkouts.swift
//  FitFeed
//
//  Created by Ryan Gutierrez on 11/7/19.
//  Copyright © 2019 FitFeedGroup. All rights reserved.
//

import Foundation
import UIKit

public class PostSubworkouts {
    var title:String
    var reps:Int
    var sets:Int
    
    init(title: String, reps: Int, sets: Int){
        self.title = title
        self.reps = reps
        self.sets = sets
    }
}
