//
//  ProfilePostViewController.swift
//  FitFeed
//
//  Created by Ryan Gutierrez on 12/2/19.
//  Copyright © 2019 FitFeedGroup. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseFirestore
import FirebaseStorage

var profSubworkoutArr:[String] = []
var profRepsArr:[String] = []
var profSetsArr:[String] = []

class ProfilePostViewController: UIViewController, UITableViewDelegate, UITableViewDataSource{

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var imageField: UIImageView!
    @IBOutlet weak var auth: UILabel!
    @IBOutlet weak var work: UILabel!
    @IBOutlet weak var descript: UILabel!
    
    var delegate: UIViewController!
    var author = String()
    var workout = String()
    var displayName = String()
    var descriptDelegate = String()
    var subworks:[String] = []
    var reps:[String] = []
    var sets:[String] = []
    
    @IBAction func deletePost(_ sender: Any) {
        let db = Firestore.firestore()
            let user = Auth.auth().currentUser?.email!
            let docTitle = "\(workout)-\(user!)"
        db.collection("posts").document(docTitle).delete() { err in
                if let err = err {
                    print("Error removing document: \(err)")
                }
            }
        self.dismiss(animated: false, completion: nil)
        
        
    }
    
    @IBAction func back(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        descript.numberOfLines = 0
        //descript.sizeToFit()
        
        auth.text = displayName
        work.text = workout
        descript.text = descriptDelegate
        
        profSubworkoutArr = subworks
        profRepsArr = reps
        profSetsArr = sets

        
        tableView.delegate = self
        tableView.dataSource = self
        
        
        let user = author
        let storage = Storage.storage()
        let storageRef = storage.reference()
        let workoutImages = storageRef.child("workoutImages")
        let userImageFolder = workoutImages.child("\(author)")
        var imageRef = userImageFolder.child("\(workout)")
        // Load the image using SDWebImage
        imageRef.getData(maxSize: 1000*1024*1024) { (data, error) in
            if (error != nil) {
               let userImages = storageRef.child("userImages")
               imageRef = userImages.child("\(user)")
                // Load the image using SDWebImage
                imageRef.getData(maxSize: 1000*1024*1024) { (data, error) in
                    if (error != nil) {
                        //print("FSB")
                    } else {
                        self.imageField.image = UIImage(data: data!)
                    }
                }
            } else {
                self.imageField.image = UIImage(data: data!)
            }
        }
        
        
        
        

        // Do any additional setup after loading the view.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return profSubworkoutArr.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PostSubCell", for: indexPath) as! ProfilePostSubsTableViewCell
        let row = indexPath.row

        cell.subworkout.text = profSubworkoutArr[row]
        cell.repsLabel.text = String(profRepsArr[row])
        cell.setsLabel.text = String(profSetsArr[row])
        return cell
    }

    override func viewDidAppear(_ animated: Bool) {
        tableView.reloadData()
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

/*@IBOutlet weak var tableView: UITableView!

@IBOutlet weak var imageField: UIImageView!

@IBAction func back(_ sender: Any) {
    self.dismiss(animated: true, completion: nil)
}
@IBOutlet weak var descript: UILabel!
@IBOutlet weak var auth: UILabel!
@IBOutlet weak var work: UILabel!

var delegate: UIViewController!
var author = String()
var workout = String()
var displayName = String()
var descriptDelegate = String()
var subworks:[String] = []
var reps:[Int] = []
var sets:[Int] = []

@IBAction func savePressed(_ sender: Any) {
    let db = Firestore.firestore()
    let user = Auth.auth().currentUser!.email
    db.collection("users/\(user!)/workouts").document("\(workout)-\(user!)").setData([
        "title": workout, "user": author, "description":descriptDelegate
                       ])
    for i in 0 ... subworks.count - 1 {
        //print(i)
        db.collection("users/\(user!)/workouts/\(workout)-\(user!)/subworkouts").document("\(subworks[i])-\(i)").setData([
            "title": subworks[i], "reps": reps[i], "sets": sets[i]
                                   ])
    }
}
@IBOutlet weak var saveButton: UIButton!
override func viewDidLoad() {
    super.viewDidLoad()
  
    descript.numberOfLines = 0
    //descript.sizeToFit()
    
    auth.text = displayName
    work.text = workout
    descript.text = descriptDelegate
    
    if author == Auth.auth().currentUser!.email! {
        saveButton.isHidden = true
    }

    
  
   
    subworkoutArr = subworks
    repsArr = reps
    setsArr = sets

    
    tableView.delegate = self
    tableView.dataSource = self
    
    
    let user = author
    let storage = Storage.storage()
    let storageRef = storage.reference()
    let userImages = storageRef.child("userImages")
    let imageRef = userImages.child("\(user)")
    // Load the image using SDWebImage
    imageRef.getData(maxSize: 1000*1024*1024) { (data, error) in
        if (error != nil) {
           // print("FSB")
        } else {
            self.imageField.image = UIImage(data: data!)
        }
    }
    
    
    
    // Do any additional setup after loading the view.
}


func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return subworkoutArr.count
}

func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "PostCell", for: indexPath) as! PostTableViewCell
    let row = indexPath.row

    cell.subworkout.text = subworkoutArr[row]
    cell.repsLabel.text = String(repsArr[row])
    cell.setsLabel.text = String(setsArr[row])
    return cell
}

override func viewDidAppear(_ animated: Bool) {
    tableView.reloadData()
}
 */
