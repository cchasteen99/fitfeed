//
//  ProfileViewController.swift
//  FitFeed
//
//  Created by Christopher Chasteen on 11/1/19.
//  Copyright © 2019 FitFeedGroup. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseFirestore
import FirebaseStorage

var mainProfileReloadVar = false

public var profilePosts:[MainPost] = []

class ProfileViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var titleItem: UINavigationItem!
    @IBOutlet weak var imageField: UIImageView!
    @IBOutlet weak var bioField: UITextView!
    @IBOutlet weak var followersCount: UILabel!
    @IBOutlet weak var followingCount: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let user = Auth.auth().currentUser?.email!
        
        let storage = Storage.storage()
        let storageRef = storage.reference()
        let userImages = storageRef.child("userImages")
        let imageRef = userImages.child("\(user!)")
        // Load the image using SDWebImage
        imageRef.getData(maxSize: 1000*1024*1024) { (data, error) in
            if (error != nil) {
                print("FSB")
            } else {
                self.imageField.image = UIImage(data: data!)
            }
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        getPost()
        var user = Auth.auth().currentUser?.email!
        print("GRABBING USER INFO")
        let db = Firestore.firestore()
        let ref = db.collection("users").document(user!)
        ref.getDocument { (snapshot, err) in
            if let data = snapshot?.data() {
                self.bioField.text=data["Bio"]! as! String
                self.titleItem.title = data["displayName"]! as! String
                // loads in followers list if available
                if data["Followers"] != nil {
                    print("followers", (data["Followers"] as! Array<Any>).count)
                    self.followersCount.text = String((data["Followers"] as! Array<Any>).count)
                }
                else {
                    print("no followers list available")
                }
                // loads in following list if available
                if data["Following"] != nil {
                    print("following", (data["Following"] as! Array<Any>).count)
                    self.followingCount.text = String((data["Following"] as! Array<Any>).count)
                }
                else {
                    print("no following list available")
                }
            } else {
                print("Couldn't find the document")
            }
        }
        
           let storage = Storage.storage()
           let storageRef = storage.reference()
           let userImages = storageRef.child("userImages")
           let imageRef = userImages.child("\(user!)")
           // Load the image using SDWebImage
           imageRef.getData(maxSize: 1000*1024*1024) { (data, error) in
               if (error != nil) {
                   print("FSB")
               } else {
                   self.imageField.image = UIImage(data: data!)
               }
           }
    }
    
    @IBAction func followersButton(_ sender: Any) {
        let db = Firestore.firestore()
        let user = Auth.auth().currentUser?.email
        let ref = db.collection("users").document(user!)
        ref.getDocument { (snapshot, err) in
            if let data = snapshot?.data() {
                followListUser = data["displayName"]! as! String
            }}
        followListState = "followers"
        mainProfileReloadVar = true
    }
    
    @IBAction func followingButton(_ sender: Any) {
        let db = Firestore.firestore()
        let user = Auth.auth().currentUser?.email
        let ref = db.collection("users").document(user!)
        ref.getDocument { (snapshot, err) in
            if let data = snapshot?.data() {
                followListUser = data["displayName"]! as! String
            }}
        followListState = "following"
        mainProfileReloadVar = true
    }
    

        
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return profilePosts.count
        }
        
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ProfilePostCell", for: indexPath) as! ProfilePostsTableViewCell
            
            let row = indexPath.row
            
            cell.subworkout1.text = ""
            cell.subworkout2.text = ""
            cell.moreSubs.isHidden = true
            
            if profilePosts[row].subworkouts.count != 0{
                if profilePosts[row].subworkouts.count == 1{
                    //print(posts[row].subworkouts.count)
                    cell.subworkout1.text = posts[row].subworkouts[0]
                }
                else if profilePosts[row].subworkouts.count == 2{
                    //print(posts[row].subworkouts.count)
                    cell.subworkout1.text = profilePosts[row].subworkouts[0]
                    cell.subworkout2.text = profilePosts[row].subworkouts[1]
                    
                }
                else{
                    //print(posts[row].subworkouts.count, "more than 2")
                    cell.subworkout1.text = profilePosts[row].subworkouts[0]
                    cell.subworkout2.text = profilePosts[row].subworkouts[1]
                    cell.moreSubs.isHidden = false
                    
                    }

            }
            
            var user = profilePosts[row].author
            let storage = Storage.storage()
            let storageRef = storage.reference()
            let userImages = storageRef.child("userImages")
            let imageRef = userImages.child("\(user)")
            // Load the image using SDWebImage
            imageRef.getData(maxSize: 1000*1024*1024) { (data, error) in
                if (error != nil) {
                    //print("FSB")
                } else {
                    cell.imageField.image = UIImage(data: data!)
                }
            }
            
            cell.descript.text = profilePosts[row].descript
            //self.view.bringSubviewToFront(cell.descript)
            cell.workout.text = profilePosts[row].workout
            //cell.workout.bringSubviewToFront(cell.workout)
            cell.userName.text = profilePosts[row].displayName
            return cell
        }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if profilePosts.count > 0{
            performSegue(withIdentifier: "profilePostSegue", sender: self)
            
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "profilePostSegue",
            
            let postVC = segue.destination as? ProfilePostViewController {
            
            postVC.delegate = self
            let postIndex = tableView.indexPathForSelectedRow?.row
            postVC.author = profilePosts[postIndex!].author
            postVC.workout = profilePosts[postIndex!].workout
            postVC.displayName = profilePosts[postIndex!].displayName
            postVC.descriptDelegate = profilePosts[postIndex!].descript
            postVC.subworks = profilePosts[postIndex!].subworkouts
            postVC.reps = profilePosts[postIndex!].reps
            postVC.sets = profilePosts[postIndex!].sets
            
        }
    }
        
        
       func getPost(){
        profilePosts = []
        tableView.reloadData()
           let db = Firestore.firestore()
        if let user = Auth.auth().currentUser?.email{
                db.collection("posts").getDocuments{ (snapshot, err) in
                       if let err = err {
                          print("Error getting documents: \(err)")
                          } else {
                          for document in snapshot!.documents {
                             let author = document.get("author") as! String
                            if author == user {
                            // Get the display name
                             let workout = document.get("title") as! String
                             let descript = document.get("description") as! String
                             let timeStamp = document.get("timestamp") as! String
                             let formatter = DateFormatter()
                                formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                             let ts = formatter.date(from:timeStamp)
                                
                            db.collection("posts").document("\(workout)-\(author)").collection("subworkouts").getDocuments(){ (snapshot, err) in
                            if let err = err {
                               print("Error getting documents: \(err)")
                               } else {
                                var subworkouts:[String] = []
                                var repsArr:[String] = []
                                var setsArr:[String] = []
                               for document in snapshot!.documents {
                                  let docId = document.documentID
                                  let subworkout = document.get("title") as! String
                                  subworkouts.append(subworkout)
                                  let reps = document.get("reps") as! String
                                repsArr.append(reps)
                                  let sets = document.get("sets") as! String
                                  setsArr.append(sets)
                                //print(subworkouts, reps, sets)
                                }
                                
                                let ref = db.collection("users").document(author)
                                ref.getDocument { (snapshot, err) in
                                    if let data = snapshot?.data() {
                                        profilePosts.append(MainPost(author: author, displayName:(data["displayName"]! as! String), workout: workout, descript: descript, subworkouts: subworkouts, reps: repsArr, sets: setsArr, timeStamp: ts!))
                                        profilePosts = profilePosts.sorted(by: {
                                            $0.timeStamp.compare($1.timeStamp) == .orderedDescending
                                        })
                                        self.tableView.reloadData()
                                    } else {
                                      
                                    }
                                }

                               
                            }
                          }
                        }
                      }
                      
                   }
                    
                }
                
        }
        
    }
            
              
    
    
}
