//
//  SearchTableViewCell.swift
//  FitFeed
//
//  Created by Laith Alsukhni on 11/5/19.
//  Copyright © 2019 FitFeedGroup. All rights reserved.
//

import UIKit

class SearchTableViewCell: UITableViewCell {
    @IBOutlet weak var profilePic: UIImageView!
    @IBOutlet weak var userName: UILabel!
    
}
