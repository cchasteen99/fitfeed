//
//  ProfilePostSubsTableViewCell.swift
//  FitFeed
//
//  Created by Ryan Gutierrez on 12/2/19.
//  Copyright © 2019 FitFeedGroup. All rights reserved.
//

import UIKit

class ProfilePostSubsTableViewCell: UITableViewCell {
    @IBOutlet weak var subworkout: UILabel!
    @IBOutlet weak var setsLabel: UILabel!
    @IBOutlet weak var repsLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
