//
//  SignUpViewController.swift
//  FitFeed
//
//  Created by Christopher Chasteen on 10/14/19.
//  Copyright © 2019 FitFeedGroup. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseFirestore
import FirebaseStorage

class SignUpViewController: UIViewController {

    @IBAction func backPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var passwordConfirm: UITextField!
    @IBAction func signUpAction(_ sender: Any) {
        if password.text != passwordConfirm.text {
        let alertController = UIAlertController(title: "Password Incorrect", message: "Please re-type password", preferredStyle: .alert)
        let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
                    
        alertController.addAction(defaultAction)
        self.present(alertController, animated: true, completion: nil)
                }
        else{
        Auth.auth().createUser(withEmail: email.text!, password: password.text!){ (user, error) in
         if error == nil {
            print("CREATING NEW USER")
           let db = Firestore.firestore()
           db.collection("users").document(self.email.text!).setData([
            "Bio": "Default Bio can be changed in settings", "Followers" : [], "Following" : ["fitfeed2019@gmail.com"], "displayName" : self.email.text!
            ])
            let ref = db.collection("users").document("fitfeed2019@gmail.com")
                       ref.getDocument { (snapshot, err) in
                           if let data = snapshot?.data() {
                               // loads in followers list if available
                               var followers = (data["Followers"] as! Array<Any>) as! [String]
                               followers.append(self.email.text!)
                               db.collection("users").document("fitfeed2019@gmail.com").updateData(["Followers" : followers])
                           } else {
                               print("Couldn't find the document")
                           }
                       }
           self.performSegue(withIdentifier: "signupToHome", sender: self)
            
            let storage = Storage.storage()
            let storageRef = storage.reference()
            let userImages = storageRef.child("userImages")
            let imageRef = userImages.child("\(user!)")
            let Data = UIImage(named: "blank-profile-picture.png")?.pngData()
            
            //Create Upload Task
            imageRef.putData(Data!, metadata: nil)
        }
         else{
           let alertController = UIAlertController(title: "Error", message: error?.localizedDescription, preferredStyle: .alert)
           let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
                            
            alertController.addAction(defaultAction)
            self.present(alertController, animated: true, completion: nil)
               }
                    }
              }
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    // code to dismiss keyboard when user clicks on background

    func textFieldShouldReturn(textField:UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
