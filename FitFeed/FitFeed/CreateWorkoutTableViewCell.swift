//
//  CreateWorkoutTableViewCell.swift
//  FitFeed
//
//  Created by Caleb Barnwell on 11/5/19.
//  Copyright © 2019 FitFeedGroup. All rights reserved.
//

import UIKit

class CreateWorkoutTableViewCell: UITableViewCell, UITextFieldDelegate {
    var sets: String = "0"
    
    @IBOutlet weak var titleField: UITextField!
    @IBOutlet weak var setField: UITextField!
    @IBOutlet weak var repField: UITextField!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        repField.delegate = self
        setField.delegate = self
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if(textField == setField){
            let maxLength = 10
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        }
        else{
            let maxLength = 20
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        }
    }
}
