//
//  ProfilePostsTableViewCell.swift
//  FitFeed
//
//  Created by Ryan Gutierrez on 12/2/19.
//  Copyright © 2019 FitFeedGroup. All rights reserved.
//

import UIKit

class ProfilePostsTableViewCell: UITableViewCell {
    
   /* @IBOutlet weak var subworkout1: UILabel!
    @IBOutlet weak var subworkout2: UILabel!
    @IBOutlet weak var moreSubs: UILabel!
    
    @IBOutlet weak var imageField: UIImageView!
    @IBOutlet weak var descript: UILabel!
    @IBOutlet weak var workout: UILabel!
    @IBOutlet weak var userName: UILabel!*/
    
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var imageField: UIImageView!
    @IBOutlet weak var workout: UILabel!
    @IBOutlet weak var descript: UILabel!
    @IBOutlet weak var subworkout1: UILabel!
    @IBOutlet weak var subworkout2: UILabel!
    @IBOutlet weak var moreSubs: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
