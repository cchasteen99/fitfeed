//
//  WorkoutViewerViewController.swift
//  FitFeed
//  EID: cmb5827
//  Course: CS371L
//
//  Created by Caleb Barnwell on 10/24/19.
//  Copyright © 2019 FitFeedGroup. All rights reserved.
//

import UIKit
import CoreData
import Firebase
import FirebaseFirestore
import FirebaseAuth
// Used to gather workout information to display
protocol WorkoutViewerDelegate {
    var workoutViewerTitle: String {get set}
    var workoutViewerSubTitle: String {get set}
    var workoutViewerSets: String {get set}
    var workoutViewerReps: String {get set}
    var workoutViewerDescription: String {get set}
    
    var subworkoutList: [Subworkout] {get set}
}

class WorkoutViewerViewController: UIViewController, EditWorkoutDelegate {
    @IBOutlet weak var workoutTitle: UILabel!
    @IBOutlet weak var descriptionField: UITextView!
    @IBOutlet weak var viewTable: UITableView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var postButton: UIButton!
    
    var editWorkoutTitle: String = ""
    var editWorkoutDescription: String = ""
    var subworkoutList: [Subworkout] = []
    var posted: Bool = false
    
    var delegate:WorkoutViewerDelegate?
    
    @IBAction func deleteWorkout(_ sender: Any) {
        let db = Firestore.firestore()
        let user = Auth.auth().currentUser?.email!
        let docTitle = "\(workoutTitle.text!)-\(user!)"
    db.collection("users/\(user!)/workouts").document(docTitle).delete() { err in
            if let err = err {
                print("Error removing document: \(err)")
            }
        }
        let storage = Storage.storage()
        let storageRef = storage.reference()
        let workoutImages = storageRef.child("workoutImages")
        let userImageFolder = workoutImages.child("\(user!)")
        let imageRef = userImageFolder.child("\(workoutTitle.text!)")
        imageRef.delete()
        
        self.dismiss(animated: true, completion: nil)
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        viewTable.rowHeight = 60
        viewTable.delegate = self
        viewTable.dataSource = self
    }
    @IBAction func `return`(_ sender: Any) {
        self.dismiss(animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        // Gather workout information
        if(delegate != nil){
            workoutTitle.text = delegate?.workoutViewerTitle
            descriptionField.text = delegate?.workoutViewerDescription
        }
        let user = Auth.auth().currentUser?.email!
        let storage = Storage.storage()
        let storageRef = storage.reference()
        let workoutImages = storageRef.child("workoutImages")
        let userImageFolder = workoutImages.child("\(user!)")
        let imageRef = userImageFolder.child("\(workoutTitle.text!)")
        imageRef.getData(maxSize: 1000*1024*1024) { (data, error) in
            if (error != nil) {
                print("FSB")
            } else {
                self.imageView.image = UIImage(data: data!)
            }
        }
        let db = Firestore.firestore()
//        let docTitle = "\(delegate!.workoutViewerTitle)-\(user!)"
        let post = db.collection("posts")
        
        post.getDocuments() { (querySnapshot, err) in
        if let err = err {
            print("Error getting documents: \(err)")
        } else {
            for subDocument in querySnapshot!.documents {
                print(subDocument.data()["title"] as! String)
                if (subDocument.data()["title"] as! String == self.delegate!.workoutViewerTitle){
                    print("found post")
                    self.posted = true
                    self.postButton.isHidden = true
                    break
                }
            }
            }
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        viewTable.reloadData()
    }
    @IBAction func post(_ sender: Any) {
        let formatter = DateFormatter()
               formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
               let date = Date()
               let fd = formatter.string(from: date)
//               save(self)
//               fetched = false
//                       subTable.reloadData()

                       if workoutTitle.text != "" {
                           let db = Firestore.firestore()
                           let user = Auth.auth().currentUser?.email!
                           let docTitle = "\(workoutTitle.text!)-\(user!)"
                           
                           db.collection("posts").document(docTitle).setData([
                               "title": self.delegate!.workoutViewerTitle, "author": user!,
                               "description": self.delegate!.workoutViewerDescription, "image_url": "www.fakeimageurl.zzz",
                               "timestamp": fd
                           ])
                        for i in 0 ..< delegate!.subworkoutList.count{
                            db.collection("posts/\(docTitle)/subworkouts").document("\(delegate!.subworkoutList[i].title)-\(i)").setData(["title": delegate!.subworkoutList[i].title, "sets": delegate!.subworkoutList[i].sets, "reps": delegate!.subworkoutList[i].reps])
                           }
                        self.postButton.isHidden = true
                       }
                       else {
                           let alert = UIAlertController(title: "Error", message: "Text Fields Cannot be blank", preferredStyle: .alert)
                           alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
                           self.present(alert, animated: true)
                       }
    }
    
    // Prepare to edit
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "editWorkoutSegue" {
            editWorkoutTitle = delegate!.workoutViewerTitle
            editWorkoutDescription = delegate!.workoutViewerDescription
            subworkoutList = delegate!.subworkoutList
            let vc : EditWorkoutViewController = segue.destination as! EditWorkoutViewController
            vc.delegate = self
        }
    }
}

extension WorkoutViewerViewController: UITableViewDataSource, UITableViewDelegate{
    
    // Return the number of rows for the table.
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //print("returning count")
        //print(delegate!.subworkoutList.count)
        return delegate!.subworkoutList.count
    }
    
    // Provide a cell object for each row.
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //print("Fetching cell")
        // Fetch cell
        let cell = viewTable.dequeueReusableCell(withIdentifier: "WorkoutViewTableViewCell") as! WorkoutViewTableViewCell
        //print("IndexPath Row: \(indexPath.row)")
        cell.subTitle.text = delegate!.subworkoutList[indexPath.row].title
        cell.setNumber.text = String(delegate!.subworkoutList[indexPath.row].sets)
        cell.repNumber.text = String(delegate!.subworkoutList[indexPath.row].reps)
        return cell
    }
    
    // Cell has been selected
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
}

