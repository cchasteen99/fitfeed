//
//  MainFeedPostViewController.swift
//  FitFeed
//
//  Created by Ryan Gutierrez on 10/31/19.
//  Copyright © 2019 FitFeedGroup. All rights reserved.
//
import UIKit
import FirebaseAuth
import FirebaseFirestore
import FirebaseStorage

var subworkoutArr:[String] = []
var repsArr:[String] = []
var setsArr:[String] = []

class MainFeedPostViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var imageField: UIImageView!
    
    @IBAction func back(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBOutlet weak var descript: UILabel!
    @IBOutlet weak var auth: UILabel!
    @IBOutlet weak var work: UILabel!
    
    var delegate: UIViewController!
    var author = String()
    var workout = String()
    var displayName = String()
    var descriptDelegate = String()
    var subworks:[String] = []
    var reps:[String] = []
    var sets:[String] = []
    
    @IBAction func savePressed(_ sender: Any) {
        let db = Firestore.firestore()
        let user = Auth.auth().currentUser!.email
        db.collection("users/\(user!)/workouts").document("\(workout)-\(user!)").setData([
            "title": workout, "user": user, "description":descriptDelegate
                           ])
        for i in 0 ... subworks.count - 1 {
            //print(i)
            db.collection("users/\(user!)/workouts/\(workout)-\(user!)/subworkouts").document("\(subworks[i])-\(i)").setData([
                "title": subworks[i], "reps": reps[i], "sets": sets[i]
                                       ])
        }
        let storage = Storage.storage()
        let storageRef = storage.reference()
        let workoutImages = storageRef.child("workoutImages")
        let userImageFolder = workoutImages.child("\(author)")
        let imageRef = userImageFolder.child("\(workout)")
        imageRef.getData(maxSize: 1000*1024*1024) { (data, error) in
            if (error != nil) {
                print("FSB")
            } else {
                let currUserImageFolder = workoutImages.child("\(user!)")
                let currImageRef = currUserImageFolder.child("\(self.workout)")
                currImageRef.putData(data!, metadata: nil)
            }
        }
        
        
    }
    @IBOutlet weak var saveButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
      
        descript.numberOfLines = 0
        //descript.sizeToFit()
        
        auth.text = displayName
        work.text = workout
        descript.text = descriptDelegate
        
        if author == Auth.auth().currentUser!.email! {
            saveButton.isHidden = true
        }

        
      
       
        subworkoutArr = subworks
        repsArr = reps
        setsArr = sets

        
        tableView.delegate = self
        tableView.dataSource = self

            
        
        let user = author
        let storage = Storage.storage()
        let storageRef = storage.reference()
        let workoutImages = storageRef.child("workoutImages")
        let userImageFolder = workoutImages.child("\(author)")
        var imageRef = userImageFolder.child("\(workout)")
        // Load the image using SDWebImage
        imageRef.getData(maxSize: 1000*1024*1024) { (data, error) in
            if (error != nil) {
               let userImages = storageRef.child("userImages")
                imageRef = userImages.child("\(self.author)")
                // Load the image using SDWebImage
                imageRef.getData(maxSize: 1000*1024*1024) { (data, error) in
                    if (error != nil) {
                        //print("FSB")
                    } else {
                        self.imageField.image = UIImage(data: data!)
                    }
                }
            } else {
                self.imageField.image = UIImage(data: data!)
            }
        }
        
        
        
        // Do any additional setup after loading the view.
    }
    

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return subworkoutArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PostCell", for: indexPath) as! PostTableViewCell
        let row = indexPath.row
    
        cell.subworkout.text = subworkoutArr[row]
        cell.repsLabel.text = String(repsArr[row])
        cell.setsLabel.text = String(setsArr[row])
        return cell
    }
    
    override func viewDidAppear(_ animated: Bool) {
        tableView.reloadData()
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
