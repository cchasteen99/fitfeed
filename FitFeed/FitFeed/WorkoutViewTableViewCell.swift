//
//  WorkoutViewTableViewCell.swift
//  FitFeed
//
//  Created by Caleb Barnwell on 11/5/19.
//  Copyright © 2019 FitFeedGroup. All rights reserved.
//

import UIKit

class WorkoutViewTableViewCell: UITableViewCell {
    @IBOutlet weak var subTitle: UILabel!
    @IBOutlet weak var setNumber: UILabel!
    @IBOutlet weak var repNumber: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
