//
//  FollowListTableViewCell.swift
//  FitFeed
//
//  Created by Laith Alsukhni on 11/20/19.
//  Copyright © 2019 FitFeedGroup. All rights reserved.
//

import UIKit

class FollowListTableViewCell: UITableViewCell {
    @IBOutlet weak var username: UILabel!
    @IBOutlet weak var profileImage: UIImageView!
}
