//
//  HomeViewController.swift
//
//
//  Created by Christopher Chasteen on 10/14/19.
//

import UIKit
import FirebaseAuth
import FirebaseFirestore
import FirebaseStorage

public var posts:[MainPost] = []

class HomeViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    
    @IBOutlet weak var refreshButtonOutlet: UIButton!
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
       super.viewDidLoad()
        self.navigationItem.setHidesBackButton(true, animated: true)
      
        //print(posts.count)
        
        tableView.delegate = self
        tableView.dataSource = self
        // Do any additional setup after loading the view.
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        getPost()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return posts.count
    }
    
    @IBAction func refresh(_ sender: Any) {
        self.refreshButtonOutlet.isEnabled = false
        getPost()
        Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(refreshTimer), userInfo: nil, repeats: true)
    }
    
    @objc func refreshTimer(){
        self.refreshButtonOutlet.isEnabled = true
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MainFeedCell", for: indexPath) as! MainFeedTableViewCell
        
        let row = indexPath.row
        
        cell.subworkout1.text = ""
        cell.subworkout2.text = ""
        cell.moreSubs.isHidden = true
        
        if posts[row].subworkouts.count != 0{
            if posts[row].subworkouts.count == 1{
                //print(posts[row].subworkouts.count)
                cell.subworkout1.text = posts[row].subworkouts[0]
            }
            else if posts[row].subworkouts.count == 2{
                //print(posts[row].subworkouts.count)
                cell.subworkout1.text = posts[row].subworkouts[0]
                cell.subworkout2.text = posts[row].subworkouts[1]
                
            }
            else{
                //print(posts[row].subworkouts.count, "more than 2")
                cell.subworkout1.text = posts[row].subworkouts[0]
                cell.subworkout2.text = posts[row].subworkouts[1]
                cell.moreSubs.isHidden = false
                
                }

        }
        
        var user = posts[row].author
        let storage = Storage.storage()
        let storageRef = storage.reference()
        let userImages = storageRef.child("userImages")
        let imageRef = userImages.child("\(user)")
        // Load the image using SDWebImage
        imageRef.getData(maxSize: 1000*1024*1024) { (data, error) in
            if (error != nil) {
                //print("FSB")
            } else {
                cell.imageField.image = UIImage(data: data!)
            }
        }
        
        cell.descript.text = posts[row].descript
        //self.view.bringSubviewToFront(cell.descript)
        cell.workout.text = posts[row].workout
        //cell.workout.bringSubviewToFront(cell.workout)
        cell.userName.text = posts[row].displayName
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if posts.count > 0{
            performSegue(withIdentifier: "mainFeedSegue", sender: self)
            self.tableView.deselectRow(at: indexPath, animated: true)
        }
        
    }
    
   func getPost(){
    posts = []
       let db = Firestore.firestore()
    if let user = Auth.auth().currentUser?.email{
    let ref = db.collection("users").document(user)
    var followList:[String] = []
    ref.getDocument { (snapshot, err) in
        if let data = snapshot?.data() {
            
            // loads in followers list if available
            if data["Following"] != nil {
                followList = data["Following"] as! Array<String>
            } else {
                followList = []
            }
            db.collection("posts").getDocuments{ (snapshot, err) in
                   if let err = err {
                      print("Error getting documents: \(err)")
                      } else {
                      for document in snapshot!.documents {
                         let author = document.get("author") as! String
                        if followList.contains(author) || author == user {
                        // Get the display name
                         let workout = document.get("title") as! String
                         let descript = document.get("description") as! String
                         let timeStamp = document.get("timestamp") as! String
                         let formatter = DateFormatter()
                            formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                         let ts = formatter.date(from:timeStamp)
                            
                        db.collection("posts").document("\(workout)-\(author)").collection("subworkouts").getDocuments(){ (snapshot, err) in
                        if let err = err {
                           print("Error getting documents: \(err)")
                           } else {
                            var subworkouts:[String] = []
                            var repsArr:[String] = []
                            var setsArr:[String] = []
                           for document in snapshot!.documents {
                              let docId = document.documentID
                              let subworkout = document.get("title") as! String
                              subworkouts.append(subworkout)
                              let reps = document.get("reps") as! String
                            repsArr.append(reps)
                              let sets = document.get("sets") as! String
                              setsArr.append(sets)
                            //print(subworkouts, reps, sets)
                            }
                            
                            let ref = db.collection("users").document(author)
                            ref.getDocument { (snapshot, err) in
                                if let data = snapshot?.data() {
                                    posts.append(MainPost(author: author, displayName:(data["displayName"]! as! String), workout: workout, descript: descript, subworkouts: subworkouts, reps: repsArr, sets: setsArr, timeStamp: ts!))
                                    posts = posts.sorted(by: {
                                        $0.timeStamp.compare($1.timeStamp) == .orderedDescending
                                    })
                                    self.tableView.reloadData()
                                } else {
                                  
                                }
                            }

                           
                        }
                      }
                    }
                  }
                  
               }
                
            }
            
        }}
        
    }
        
          
}
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
            if segue.identifier == "mainFeedSegue",
                
                let postVC = segue.destination as? MainFeedPostViewController {
                postVC.delegate = self
                let postIndex = tableView.indexPathForSelectedRow?.row
                    print(posts.count)
                    postVC.author = posts[postIndex!].author
                    postVC.workout = posts[postIndex!].workout
                    postVC.displayName = posts[postIndex!].displayName
                    postVC.descriptDelegate = posts[postIndex!].descript
                    postVC.subworks = posts[postIndex!].subworkouts
                    postVC.reps = posts[postIndex!].reps
                    postVC.sets = posts[postIndex!].sets
//            tableView.deselectRow(at: tableView.indexPathForSelectedRow!, animated: false)

            }
            
        

    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
