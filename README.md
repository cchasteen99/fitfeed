# FitFeed

Group number: 2
Team members: Laith Alsukhni, Christopher Chasteen, Caleb Barnwell, Ryan Gutierrez
Name of project: FitFeed
Dependencies: Xcode 11.1, Swift 5
Special Instructions:
• You have to open the file FitFeed.xcworkspace (as opposed to the file
FitFeed.xcodeprog).
• Use an iPhone 11 pro Simulator
• Before running the app, run "pod install" inside the FitFeed folder
where the podfile is located
• To login as admin, use login: fitfeed2019@gmail.com password: WorkOutApp!

| Feature             | Description                                | Release Planned | Release Actual | Deviations | Who Worked On         |
|---------------------|--------------------------------------------|-----------------|----------------|------------|-----------------------|
| Login               | Sign in/Sign Up                            |      Alpha           |        Alpha        |     None        |     Chris Chasteen (100%)                 |
| Profile             |           Can view your own profile and posts                   |       Beta          |    Beta/Final            |     Implemented the Profile viewer by the Beta, posts implemented by Final       | Caleb Barnwell (10%), Chris Chasteen(50%), Ryan Gutierrez(%40)  |
| Home Feed           |                       Shows posts from people you follow and yourself                     |         Beta        |    Beta            |  Fixed bugs for final           |         Ryan( Gutierrez(95%), Chris Chasteen (5%)      |
| Create Workout      | The page and process of creating a workout | Alpha           |  Beta         |   Fixed bugs for final and added images         | Caleb Barnwell (80%), Chris Chasteen(20%)  |
| My Workouts         | The page listing all saved workouts        | Alpha           | Alpha          |      None      | Caleb Barnwell (100%) |
| Workout Timer       | The workout timer utility                  | Final           | Final          |       None     | Caleb Barnwell (100%) |
| Workout Viewer      | The page that shows a saved workout        | Beta            | Beta           |   None         | Caleb Barnwell (90%)  |
| Followers/Following |               Allowed user to view lists of who they follow,and who follows them                              | Beta                 |          Beta      |   None         |       Laith  Alsukhni  (100%)             |
| Settings Page      | Allow users to edit/delete profile        | Beta            | Beta           |     None       | Chris Chasteen (100%)  |
| View Post       | Allow users to click on a post to view all of its information        | Beta            | Beta           |       None     |  Ryan Gutierrez (95%) Chris Chasteen (5%)  |
| Search Page | The page where users can search for others      | Beta            | Beta           |    None        |  Laith Alsukhni (100%)  |
| Secondary Profile Page | The page for other users' profiles        | Beta            | Beta           |     None       |  Laith Alsukhni (100%)  |
# Final

Contributions:

Christopher Chasteen (25%)
• Allowed users to edit username 
• Added images to posts and post viewers
• Save a posted workout to personal workout page
    
Caleb Barnwell (20%)
• Added images for workouts and posts
• Added a way to post workouts that have been saved but not posted
• Added the workout timer
• Adjusted workout view layouts to fit images

Laith Alsukhni (25%)
• Linked users on followers/following list result to the profile page of the user
• Fixed settings page
• Fixed general bugs
    
Ryan Gutierrez(30%)
• Added posts to the profile viewer
• Added delete post feature on profile post viewer
• Formatted/cleaned up post UI

Deviations:
• Posts do not feature likes/dislikes because we decided it was not priority given our time constraints
• Edit Workout was not fully implemented and was removed to avoid app crashing with time constraints

# Beta


Contributions:

Christopher Chasteen (30%)
    • Created Firebase Firestore for storing data in the backend
    • Created Firebase storage for holding images
    • Connected Account Creation and edit profile to work dynamically using the firestore
    • Ironed out bugs allowing users to create a new workout when clicking save
    • Implemented Save and Post button, by storing workout to posts section of firestore
    • Implmented the delete workout feature, by using the trashcan on view workout page
    • Filtered the serach page not to include your own profile
    
Caleb Barnwell (20%)
   • Converted all workout data from CoreData to Firebase Firestore database
        • Had to send data upon a user creating a workout
        • Save button will show data within MyWorkouts while post will make available for feed
        • Pull data from Firestore and display within workout list and workout view
           • Had to edit the functionality of this from alpha
   • Implemented multiple subworkouts within the workout creation
   • Added description capability

Laith Alsukhni (20%)
   • Created SearchPage and Search feature
   • Created Secondary User Profile Page
   • Linked search to find and open other users pages
   • Implemented Following/Followers lists in Firebase and linked it to profile pages
   • Implemented Follow/Unfollow functionality, updating both the user following and user followed 
    
Ryan Gutierrez(30%)
    • Implemented Main Post Feed on home page
    • Routed data from posts to view post page
    • Created Post page view controller
    • Implemented photos to appear on feed page (Firebase Storage was down on day of submission)
    
    
    
 

Deviations:
• MAJOR DEVIATION - The day of the due date for the Beta release, our back end image storage (Firebase Storage), has crashed and we currently cannot use any of its features. There was an outtage reported monday that did not affect us then, but we beleive we are being affected by the tail end of these issues, which the support site mentioned as a possibility. This means that ONLY our images that we store online are not being saved correctly. This affects the change profile image feature, as well as the posts and search page which include user images. If the storage comes back online before the app is graded it may still contain our old user photos and evertyhing may work as intended, or every profile will have no image, but editing user image should once again work. We are sorry for the confusion, and hope this resolves soon,  no other featuers are affected.
• Not Technically A deviation, but after comments on the Alpha we realized we did not include a timeline for creation of the workout timer, we plan to have this as part of the final release, as we believe it is a convenient feature, but not integral to the functionality and purpose of the app.
• Currently posts do not have likes or dislikes, or a way to view a posters profile from them, we plan to add this in next phase and it should be pretty similar to the profile view used for searching users.
• Users cannot currently store a downloaded post to their personal account, should be a easy    add next phase.  
• Need to add code for editing workouts
• Need to reinforce error checking for created workouts

# Alpha
Contributions:

Christopher Chasteen (30%)
    • Majority of the Login screen and firebase implementation
    • Tab Bar View Controller and skeleton pages for all of its segues
    • Some of the edit profile screen
    • Added Alerts and error checking to workout creation
    
Caleb Barnwell (35%)
    • Designed Screens
        • Create Workout Screen
        • Workout List Screen
        • Workout Display Screen
        • Large portion of Profile screen
    • Created backend database functionality for the workout creation
        • Uses coredata for alpha
        • Displays created workouts in the list page
            • (Later will also be able to post to feed/profile & add images)
    • Passed data using protocols/delegates for workout display screen

Laith Alsukhni (20%)
    • Color and style for all pages
    • Edit Profile page photo select from camera roll and edit
    • Fixed segue issues for a couple of pages
    
Ryan Gutierrez(15%)
    • Design and implementation of Splash Screen and logo
    • Created settings page

Deviations:
• We had planned to get workout creation completly finished, but dynamic creation of multiple subworkouts will take more time, and should be completed in the next phase. This will include the addition of a button to add more subworkouts to a workout before saving. This will be done as we switch workouts to saving to a database rather than using core data to show functionality as it currently does. We will also add the ability to delete a workout in this upcoming phase.
• No other major deviations, our app was what we expected for the "offline release", where we aimed to complete the non social functions of the app.
